/*
You are presented with an 8 by 8 matrix representing the positions of pieces on a chess board. 
The only pieces on the board are the black king and various white pieces. Given this matrix, 
determine whether the king is in check.

e.g.

[
[...K....],
[........],
[.B......],
[......P.],
[.......R],
[..N.....],
[........],
[.....Q..]
]

return True, since the bishop is attacking the king diagonally.
*/

var kingInCheck = (matrix) => {
  var kingPosition = [];

  for (var i = 0; i < matrix.length; i++) {
    var temp = matrix[i][0].split('');
    for (var j = 0; j < matrix.length; j++) {
      if (temp[j] === 'K') {
        kingPosition = [i,j];
      }    
    }     
  }


}


kingInCheck([
['...K....'],
['........'],
['.B......'],
['......P.'],
['.......R'],
['..N.....'],
['........'],
['.....Q..']
])