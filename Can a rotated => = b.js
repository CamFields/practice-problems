//Given two strings A and B, return whether or not
//A can be shifted some number of times to get B.
//e.g.
//If A is abcde and B is cdeab, return true. 
//If A is abc and B is acb, return false.

function rotateString(a, b) {
  if (a.length !== b.length) {
    return false;
  }
  b = b + b;
  return b.includes(a)
}

rotateString('abc', 'caab')
