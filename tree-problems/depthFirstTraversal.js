/*
Given the root node of a binary tree, print values by depth.
*/
/************************************************************************/
class Node {
  constructor(val) {
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

const a = new Node('a');      //             a
const b = new Node('b');      //            / \
const c = new Node('c');      //           b   c
const d = new Node('d');      //         /  \   \
const e = new Node('e');      //        d   e    f
const f = new Node('f');

a.left = b;
a.right = c;
b.left = d;
b.right = e;
c.right = f;
/************************************************************************/
const printDepthFirst = (root) => {
  let result = [];
  let stack = [ root ];

  while (stack.length) {
    let current = stack.pop();
    result.push(current.val);
    if (current.right) {
      stack.push(current.right);
    }
    if (current.left) {
      stack.push(current.left)
    }
  }

  return result
}

console.log(
  "printDepthFirst(a) => ['a', 'b', 'd', 'e', 'c', 'f']",
  JSON.stringify(printDepthFirst(a)) === JSON.stringify(['a', 'b', 'd', 'e', 'c', 'f'])
)
/*----------------------------------------------------------------------*/
const printDepthFirstRecursively = (root) => {
  const result = [];

  const loop = (node) => {
    if (!node) {
      return;
    }

    result.push(node.val)
    loop(node.left)
    loop(node.right)
  }
  loop(root)
  return result
}


console.log(
  "printDepthFirstRecursively(a) => ['a', 'b', 'd', 'e', 'c', 'f']",
  JSON.stringify(printDepthFirstRecursively(a)) === JSON.stringify(['a', 'b', 'd', 'e', 'c', 'f'])
)
