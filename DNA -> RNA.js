var sampleDNA = 'GCAT-GCAT-GCAT';
function dnaToRna(DNA) {
  var input = DNA.split('');
  for (var i = 0; i < input.length - 1; i++) {
    if (input[i] === 'T') {
      input[i] = 'U'
    }
  }
  return input.join('');
}
dnaToRna('GCAT-GCAT-GCAT');