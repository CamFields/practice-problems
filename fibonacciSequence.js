/*
input a fibonacci number and output an array of all numbers in the sequence preceding it.
*/
/*------------------------------O(n)------------------------------------*/
var fibonacciPrinter = (input) => {
  let outputArray = [0, 1];
  let num1 = 0;
  let num2 = 1;

  if (input === 1) {
    return [0, 1, 1];
  }

  while (outputArray[outputArray.length - 1] !== input) {
    let sum = num1 + num2
    outputArray.push(sum);
    num1 = num2;
    num2 = sum;
  }

  return outputArray
}
/*--------------------------------TESTS---------------------------------*/
console.log('fibonacciPrinter(34) => [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]', JSON.stringify(fibonacciPrinter(34)) === JSON.stringify([0, 1, 1, 2, 3, 5, 8, 13, 21, 34]))
console.log('fibonacciPrinter(2) => [0, 1, 1, 2]', JSON.stringify(fibonacciPrinter(2)) === JSON.stringify([0, 1, 1, 2]))
